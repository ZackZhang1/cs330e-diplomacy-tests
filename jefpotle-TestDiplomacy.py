#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -------------
# TestDiplomacy
# -------------

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_diplomacy_1(self):
        r = StringIO("A London Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\n")

    def test_diplomacy_2(self):
        r = StringIO("A Madrid Support B\n B London Hold\n C Paris Move Madrid\n D NewYork Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_3(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")
    
    def test_diplomacy_4(self):
        r = StringIO("A Madrid Support B\n B London Hold\n C Paris Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC [dead]\n")
# ---
# main
# ----

if __name__ == "__main__":
    main()


""" #pragma: no cover

$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1

cat TestDiplomacy.tmp
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          64      0     36      0   100%
TestDiplomacy.py      26      0      2      1    96%   58->exit
--------------------------------------------------------------
TOTAL                 90      0     38      1    99%
"""